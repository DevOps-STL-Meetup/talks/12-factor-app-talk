# 12-factor applications

## Background 
- This is an opinionated methodology for creating web applications that are easy to run in a PaaS environment.
- Heroku "distilled" best pratices to help developers build and maintain web applications "with portability and resilience when deployed to the web." 
- some of these feel odd and out of place they are circa 2011


### What
- Declarative, both in setting up the application for development and production
- Service based, allowing resources to swapped out and changed out with the application code needing to change
- allows for good app arch long term
- violations with more enterprise apps due to just not being thought of like that
    - caching data in from db long start times


app not sysadmins
Is your enterprise wanting to move to the "the cloud"? 
huge legacy portfolio of applications - evaluate and understand the cloud is not just a lift and shift
applications written 10 or more years ago weren't written with these constraints



## Codebase
_One codebase tracked in revision control, many deploys_

- Git, VCS/Azure Devops, SVN. The idea behind this is to make sure that everyone is working from the same code.
- All developers should be checking code in frequently, integrating any upstream changes with their local code and confirming all tests pass
- Ideally you are integrating at least daily if not sooner, this leads to good CI practices.
- If your application consists of multiple repositories, you've already violated this factor
- Think through the boundaries of your application
- Each application/service must be comprised in a single repository 

## Dependencies
_Explicitly declare and isolate dependencies_
- It works on my box.....
- Two things here: declare and isolate
  - declare means clearly state the dependency package and its version
  - isolate is sometimes referred to as "vendoring". Make sure that your code is not dependent on globally installed packages on a system (e.g. virtualenv or bundler)
- Never rely on system-wide packages because you can't trust them unless you own them, they may not be the right versions, you can't necessarily update them without breaking something else
- We are increasingly moving to environments that are not managed by us (e.g cloud)
- Containers assist in solving this problem by allowing you to build isolated and declarative runtimes.  

## Config
_Store config in the environment_
- along side app, particular components that change 
- can we spin up new environments without committing changes to the code repository?  Can you opensource your code today?
- Software should be able to read configurations from the env.  Store configurations in something like vault 
- google/apis/dns name changes/ docker hub name change?

## Backing services
_Treat backing services as attached resources_
- DB, cache, Rabbit, SMTP
- URL/Endpoint local or remote or SAAS
- Easy to rip out change out without any code changes

## Build, release, run
_Strictly separate build and run stages_
- Do all the heavy lifting at build time so that run time is simple
- A good example of this would be gathering dependencies and compiling Java/Go code during build, pushing the code to the executing environment, confirming the necessary config is inserted into the environment and then executing the compiled code.  

## Processes
_Execute the app as one or more stateless processes_
- STATELESS!! Don't keep even small bits of application data along side the appication
- Fault tolerant
- Horizontal Scaling
- You could use local memory or disk to store a large file upload, manipulate it, and store it in blob storage or the database post transaction.  Your application should never assume that anything cached locall will be available in the future.
- Sessions

## Port binding
_Export services via port binding_
- Separation of applications
    - no mod rewrites
- expose a http service for other services to consume via a DNS name and port
- Allows for load balancing

## Concurrency
- Scale out via the process model
- Scale parts of the applicaiton that makes sense
- We shouldn't need just the biggest baddest box anymore, we should handle the work dynamically and use services in the cloud to allow us to do this.  
- Your app should not try to do it’s own process management or daemonizing, but instead rely on a process manager (run in the foreground)

## Disposability
_Maximize robustness with fast startup and graceful shutdown_
- nothing to stop and process when shutting down
- pets vs cattle, configuration changes can restart your application immediately, scaling can happen instantly
- quick shutdown, based upon sigterms and robust enought to handle sudden death
- who remembers Microsoft Word crashing, or outlook crashing and having to rebuild its database?

## Dev/prod parity
_Keep development, staging, and production as similar as possible_
- Keep envs close
- not only infra, but also releases
- we should be deploying as fast as the enterprise will allow
- things like dev using apache, prod using nginx | qa using DNS loadbalancing, prod using F5's
- don't do things like use sqlite locally and mysql in production... things like containers really help give you dev/prod parity (remember installing mysql on a laptop?)
- developing java app on windows laptop to deploy to a linux machine in production (different webservers?)


## Logs
_Treat logs as event streams_
- If we can see what we are doing then we don't really know the health of the application
- we shouldn't only be using the logs for break/fix but for understanding how our app is really doing
- this also extends to (real) metrics - how fast is our app preforming. who cares about CPU%, record too, but focus on meaningful things like latency, reqs/s, logins, find one metric that indicates overall health. (facebook monitors # of posts/second, netflix monitors play button pushes)
- send them to stdout, let other tools (elasticsearch, splunk, etc.) pick them up from there

## Admin processes
_Run admin/management tasks as one-off processes_
- Things like DB migrations
- Things like logging in and running one offs isn't something acceptable for todays enterprise cloud providers.... REPL is bad.
possibly create a new service with the admin process exctracted from the code, and run only when needed
    - Nightly cleanup
    - batch processing of data 
Possibly even a Rest endpoint exposing that





